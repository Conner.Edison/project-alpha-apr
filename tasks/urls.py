from django.urls import path
from tasks.views import (
    TaskCreateView,
    MyTasksListView,
    TaskUpdateView,
)

urlpatterns = [
    path("create/", TaskCreateView.as_view(), name="create_task"),
    path("mine/", MyTasksListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
